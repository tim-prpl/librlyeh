MACHINE = $(shell $(CC) -dumpmachine)
SHELL = /bin/bash

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv)

HEADERS = $(wildcard $(INCDIR)/$(TARGET)/*.h)
SOURCES = $(wildcard $(SRCDIR)/rlyeh*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes \
          --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) \
		  -fprofile-arcs -ftest-coverage \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500 \
		  $(shell pkg-config --cflags cmocka) -D_FILE_OFFSET_BITS=64
LDFLAGS += -fprofile-arcs -ftest-coverage \
           -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) -lamxc -lamxj -locispec -lsahtrace -lgpgme

CFLAGS += $(shell pkg-config --cflags yajl libcurl openssl gpgme)
LDFLAGS += $(shell pkg-config --libs yajl libcurl openssl gpgme)

ifeq ($(CC_NAME),g++)
	CFLAGS += -std=c++2a
else
	CFLAGS += -Wstrict-prototypes -Wold-style-definition -Wnested-externs -std=gnu99
endif