/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>

#include <amxc/amxc.h>
#include <rlyeh/rlyeh.h>
#include <rlyeh_copy_priv.h>

#include <debug/sahtrace.h>

#include "test_rlyeh_copy.h"

#define UNUSED __attribute__((unused))

static void oops(const char* s1, const char* s2) {
    fprintf(stderr, "Error: %s ", s1);
    perror(s2);
    exit(1);
}

static void copy_file(const char* src, const char* dest, const char* dest_dir) {
#define BUFFERSIZE 128
#define COPYMODE 00755
    int in_fd = 0;
    int out_fd = 0;
    int n_chars = 0;
    char buf[BUFFERSIZE];
    DIR* dir_ptr;

    if((in_fd = open(src, O_RDONLY)) == -1) {
        oops("Cannot open ", src);
    }
    if(( dir_ptr = opendir(dest_dir)) == NULL) {
        fprintf(stderr, "ls1: cannot open %s\n", dest_dir);
    } else {
        if((out_fd = creat(dest, COPYMODE)) == -1) {
            oops("Cannot creat", dest);
        }
    }
    closedir(dir_ptr);

    while((n_chars = read(in_fd, buf, BUFFERSIZE)) > 0) {
        if(write(out_fd, buf, n_chars) != n_chars) {
            oops("Write error to ", src);
        }
    }
    if(n_chars == -1) {
        oops("Read error from ", src);
    }

    if((close(in_fd) == -1) || (close(out_fd) == -1)) {
        oops("Error closing files", "");
    }
}

// Returns true if the files are the same.
static bool compareFile(const char* fname1, const char* fname2) {
    bool ret = false;
    char ch1, ch2;
    FILE* fd1 = fopen(fname1, "r");
    FILE* fd2 = fopen(fname2, "r");

    if((fd1 == NULL) || (fd2 == NULL)) {
        return false;
    }

    do {
        ch1 = fgetc(fd1);
        ch2 = fgetc(fd2);
        if(ch1 != ch2) {
            ret = false;
            goto exit;
        }
    } while(ch1 != EOF && ch2 != EOF);

    if((ch1 == EOF) && (ch2 == EOF)) {
        ret = true;
    } else {
        ret = false;
    }

exit:
    if(fd1) {
        fclose(fd1);
    }
    if(fd2) {
        fclose(fd2);
    }

    return ret;
}

int test_rlyeh_copy_setup(UNUSED void** state) {
    return rlyeh_curl_init();
}
int test_rlyeh_copy_teardown(UNUSED void** state) {
    rlyeh_curl_cleanup();
    return 0;
}

void test_rlyeh_copy(UNUSED void** state) {
    rlyeh_status_t status = RLYEH_NO_ERROR;
    rlyeh_copy_data_t data;
    char err_msg[RLYEH_ERR_MSG_LEN] = "";


    // Create images and blobs dir
    mkdir("/tmp/images", 0755);
    mkdir("/tmp/blobs", 0755);

    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_copy");
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, "Generic");
    sahTraceOpen("RLYEH_TEST", TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO("Generic", "sahTrace initialized");

    // Data initialization
    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://registry-1.docker.io/library/alpine:latest");
    data.destination = strdup("oci:/tmp/images/library/alpine:latest");
    data.dest_shared_blob_dir = strdup("/tmp/blobs");

    status = rlyeh_copy(&data, err_msg);
    assert_int_equal(status, 0);

    // Test image already exists
    status = rlyeh_copy(&data, err_msg);
    assert_int_equal(status, 0);

    rlyeh_copy_data_clean(&data);

    // Test with wrong format in destination
    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://docker-prpl.sahrd.io/qa/smallshell_rpi_oci:v1");
    data.destination = strdup("docker:/tmp/images/smallshell_rpi_oci:v1");
    data.dest_shared_blob_dir = strdup("/tmp/blobs");
    status = rlyeh_copy(&data, err_msg);
    assert_int_equal(status, 2);  // <-- invalid arguments
    rlyeh_copy_data_clean(&data);

    // Test with wrong credentials
    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://docker-prpl.sahrd.io/qa/smallshell_rpi_oci:v1");
    data.destination = strdup("oci:/tmp/images/smallshell_rpi_oci:v1");
    data.dest_shared_blob_dir = strdup("/tmp/blobs");
    data.user = strdup("user:pwd");
    status = rlyeh_copy(&data, err_msg);
    assert_int_equal(status, 5);  // <-- download failed due to wrong credentials
    rlyeh_copy_data_clean(&data);

    // Add test image from docker.v3d.fr
    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://docker-prpl.sahrd.io/qa/smallshell_rpi_oci:v1");
    data.destination = strdup("oci:/tmp/images/smallshell_rpi_oci:v1");
    data.dest_shared_blob_dir = strdup("/tmp/blobs");
    status = rlyeh_copy(&data, err_msg);
    assert_int_equal(status, 0);

    // Test avoid re-download blobs
    remove("/tmp/images/smallshell_rpi_oci/index.json");
    remove("/tmp/images/smallshell_rpi_oci/oci-layout");
    rmdir("/tmp/images/smallshell_rpi_oci");
    status = rlyeh_copy(&data, err_msg);
    assert_int_equal(status, 0);
    rlyeh_copy_data_clean(&data);

    // Remove images and blobs
    remove("/tmp/images/library/alpine/index.json");
    remove("/tmp/images/library/alpine/oci-layout");
    rmdir("/tmp/images/library/alpine");
    rmdir("/tmp/images/library");

    remove("/tmp/images/smallshell_rpi_oci/index.json");
    remove("/tmp/images/smallshell_rpi_oci/oci-layout");
    rmdir("/tmp/images/smallshell_rpi_oci");

    remove("/tmp/blobs/sha256/59bf1c3509f33515622619af21ed55bbe26d24913cedbca106468a5fb37a50c3");
    remove("/tmp/blobs/sha256/c059bfaa849c4d8e4aecaeb3a10c2d9b3d85f5165c66ad3a4d937758128c4d18");
    remove("/tmp/blobs/sha256/e7d88de73db3d3fd9b2d63aa7f447a10fd0220b7cbf39803c803f2af9ba256b3");
    remove("/tmp/blobs/sha256/722d06b0810bd903c6dc985db3331c788008fee1ef0ea9a48b1bd0a837d55892");
    remove("/tmp/blobs/sha256/47046d47361fd624bd80d54e61d4ec7b1a92a06afe2d4b8bded9319d428558b4");
    remove("/tmp/blobs/sha256/fbaa26ed00e83b33bf2c7b6abe3ce24cf74da3b0cde3e6e44215aa1d3e0f1e46");
    rmdir("/tmp/blobs/sha256");

    rmdir("/tmp/images");
    rmdir("/tmp/blobs");

    sahTraceClose();
}

void test_rlyeh_size_check(UNUSED void** state) {
    rlyeh_status_t status = RLYEH_NO_ERROR;

    status = size_check(1024, 1024);
    assert_int_equal(status, RLYEH_NO_ERROR);

    status = size_check(0, 1024);
    assert_int_equal(status, RLYEH_ERROR_SIZE_CHECK);
}

void test_rlyeh_set_tmp_blob_filename(UNUSED void** state) {
    rlyeh_status_t status = RLYEH_NO_ERROR;
    amxc_string_t filename;
    amxc_string_init(&filename, 0);

    rlyeh_copy_output_directories_t output_dir;
    rlyeh_copy_output_directories_init(&output_dir);
    amxc_string_set(&output_dir.blobs, "blobs");
    amxc_string_set(&output_dir.image, "image");

    status = set_tmp_blob_filename("sha256:hash", &output_dir, &filename, NULL);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_string_equal(filename.buffer, "blobs/sha256/hash.tmp");

    rlyeh_copy_output_directories_clean(&output_dir);
    amxc_string_clean(&filename);
}

void test_rlyeh_get_authenticate_element(UNUSED void** state) {
    char* realm = NULL;
    char* service = NULL;
    char* scope = NULL;

    const char correct_buf[1024] = "HTTP/1.1 401 Unauthorized\r\ncontent-type: application/json\r\ndocker-distribution-api-version: registry/2.0\r\nwww-authenticate: Bearer realm=\"https://auth.docker.io/token\",service=\"registry.docker.io\",scope=\"repository:library/alpine:pull\"\r\ndate: Tue, 07 Dec 2021 09:32:08 GMT\r\ncontent-length: 157\r\nstrict-transport-security: max-age=31536000\r\ndocker-ratelimit-source: 84.198.211.186\r\n";
    get_authenticate_element(correct_buf, &realm, &service, &scope);
    assert_string_equal(realm, "https://auth.docker.io/token");
    assert_string_equal(service, "registry.docker.io");
    assert_string_equal(scope, "repository:library/alpine:pull");
    if(realm) {
        free(realm);
        realm = NULL;
    }
    if(service) {
        free(service);
        service = NULL;

    }
    if(scope) {
        free(scope);
        scope = NULL;

    }

    const char noauth_buf[1024] = "HTTP/1.1 401 Unauthorized\r\ncontent-type: application/json\r\ndocker-distribution-api-version: registry/2.0\r\ndate: Tue, 07 Dec 2021 09:32:08 GMT\r\ncontent-length: 157\r\nstrict-transport-security: max-age=31536000\r\ndocker-ratelimit-source: 84.198.211.186\r\n";
    get_authenticate_element(noauth_buf, &realm, &service, &scope);
    assert_null(realm);
    assert_null(service);
    assert_null(scope);
    if(realm) {
        free(realm);
    }
    if(service) {
        free(service);
    }
    if(scope) {
        free(scope);
    }
}

void test_send_file_manifest(UNUSED void** state) {
    amxc_string_t fname;
    amxc_string_t new_name;
    amxc_string_init(&fname, 0);
    amxc_string_init(&new_name, 0);

    amxc_string_set(&fname, "manifest");
    amxc_string_set(&new_name, "new_manifest");

    copy_file("manifest.orig", "manifest", ".");
    assert_false(send_file_manifest(&fname, &new_name));
    assert_true(file_exists(new_name.buffer));
    assert_true(compareFile("manifest.orig", new_name.buffer));

    remove(new_name.buffer);

    amxc_string_clean(&fname);
    amxc_string_clean(&new_name);

}