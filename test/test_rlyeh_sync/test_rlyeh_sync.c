/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <rlyeh/rlyeh.h>
#include <rlyeh/rlyeh_sync.h>

#include "test_rlyeh_sync.h"

#define UNUSED __attribute__((unused))

void test_rlyeh_sync_wrong(UNUSED void** state) {
    rlyeh_sync_data_t data;
    init_rlyeh_sync_data(&data);

    rlyeh_sync("wrongdir", "blobs", "image", &data);
    assert_int_equal(0, data.len);
    assert_null(data.images);
    free_rlyeh_sync_data(&data);
}

void test_rlyeh_sync_null(UNUSED void** state) {
    free_rlyeh_sync_data(NULL);
    free_image_data(NULL);
}

void test_rlyeh_sync(UNUSED void** state) {
    rlyeh_sync_data_t data;

    init_rlyeh_sync_data(&data);
    rlyeh_sync("images", "blobs", "image", &data);
    assert_int_equal(1, data.len);
    assert_string_equal(data.images[0]->duid, "duid");
    assert_string_equal(data.images[0]->uri, "uri");
    assert_string_equal(data.images[0]->image_info.name, "image");
    assert_string_equal(data.images[0]->image_info.disklocation, "image");
    assert_string_equal(data.images[0]->image_info.version, "version");
    assert_string_equal(data.images[0]->image_info.vendor, "");
    assert_string_equal(data.images[0]->image_info.description, "");
    assert_false(data.images[0]->markforremoval);

    free_rlyeh_sync_data(&data);
}

void test_rlyeh_sync_subdirs(UNUSED void** state) {
    rlyeh_sync_data_t data;

    init_rlyeh_sync_data(&data);
    rlyeh_sync("images", "blobs", "subdir1/subdir2/image", &data);
    assert_int_equal(1, data.len);
    assert_string_equal(data.images[0]->duid, "duid");
    assert_string_equal(data.images[0]->uri, "uri");
    assert_string_equal(data.images[0]->image_info.name, "subdir1/subdir2/image");
    assert_string_equal(data.images[0]->image_info.disklocation, "subdir1/subdir2/image");
    assert_string_equal(data.images[0]->image_info.version, "version");
    assert_string_equal(data.images[0]->image_info.vendor, "");
    assert_string_equal(data.images[0]->image_info.description, "");
    assert_false(data.images[0]->markforremoval);

    free_rlyeh_sync_data(&data);
}

void test_rlyeh_sync_subdirs_long(UNUSED void** state) {
    rlyeh_sync_data_t data;

    init_rlyeh_sync_data(&data);
    rlyeh_sync("images", "blobs", "subdir1/subdir2/imagelonglonglonglonglonglonglonglonglonglonglonglong", &data);
    assert_int_equal(1, data.len);
    assert_string_equal(data.images[0]->duid, "duid");
    assert_string_equal(data.images[0]->uri, "uri");
    assert_string_equal(data.images[0]->image_info.name, "subdir2/imagelonglonglonglonglonglonglonglonglonglonglonglong");
    assert_string_equal(data.images[0]->image_info.disklocation, "subdir1/subdir2/imagelonglonglonglonglonglonglonglonglonglonglonglong");
    assert_string_equal(data.images[0]->image_info.version, "version");
    assert_string_equal(data.images[0]->image_info.vendor, "");
    assert_string_equal(data.images[0]->image_info.description, "");
    assert_false(data.images[0]->markforremoval);

    free_rlyeh_sync_data(&data);
}

void test_rlyeh_sync_name_from_manifest(UNUSED void** state) {
    rlyeh_sync_data_t data;

    init_rlyeh_sync_data(&data);
    rlyeh_sync("images", "blobs", "image_manifestname", &data);
    assert_int_equal(1, data.len);
    assert_string_equal(data.images[0]->duid, "duid");
    assert_string_equal(data.images[0]->uri, "uri");
    assert_string_equal(data.images[0]->image_info.name, "bench_tinymembench_sah");
    assert_string_equal(data.images[0]->image_info.disklocation, "image_manifestname");
    assert_string_equal(data.images[0]->image_info.version, "version");
    assert_string_equal(data.images[0]->image_info.vendor, "sah");
    assert_string_equal(data.images[0]->image_info.description, "bench_tinymembench container for sah");
    assert_false(data.images[0]->markforremoval);

    free_rlyeh_sync_data(&data);
}


void test_rlyeh_sync_subdirs(void** state);
void test_rlyeh_sync_subdirs_long(void** state);
void test_rlyeh_sync_name_from_manifest(void** state);
