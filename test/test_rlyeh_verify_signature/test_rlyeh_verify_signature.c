/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <gpgme.h>

#include <amxc/amxc.h>
#include <rlyeh/rlyeh.h>
#include <rlyeh_verify_signature_priv.h>

#include "test_rlyeh_verify_signature.h"
#include "rlyeh_defines_priv.h"

#define UNUSED __attribute__((unused))

void test_signature_content_verification(UNUSED void** state) {
    int ret = -1;
    rlyeh_signature_data_t sigdata;
    rlyeh_signature_data_init(&sigdata);

    sigdata.server = strdup("docker.v3d.fr");
    sigdata.image_name = strdup("zigbee_signed");
    sigdata.version = strdup("latest");
    sigdata.manifest_hash = strdup("47046d47361fd624bd80d54e61d4ec7b1a92a06afe2d4b8bded9319d428558b4");
    ret = signature_content_verification("signature.json", &sigdata);
    assert_int_equal(ret, 0);

    ret = signature_content_verification("bad_signature.json", &sigdata);
    assert_int_not_equal(ret, 0);

    ret = signature_content_verification("bad_file.json", &sigdata);
    assert_int_not_equal(ret, 0);

    rlyeh_signature_data_clean(&sigdata);
}

void test_check_content(UNUSED void** state) {
    int ret = -1;
    amxc_var_t* config_data = NULL;
    amxc_var_t* critical = NULL;
    rlyeh_signature_data_t sigdata;
    rlyeh_signature_data_init(&sigdata);
    sigdata.server = strdup("docker.v3d.fr");
    sigdata.image_name = strdup("zigbee_signed");
    sigdata.version = strdup("latest");
    sigdata.manifest_hash = strdup("47046d47361fd624bd80d54e61d4ec7b1a92a06afe2d4b8bded9319d428558b4");

    config_data = read_config("signature.json");
    critical = GET_ARG(config_data, "critical");

    ret = check_docker_reference(critical, &sigdata);
    assert_int_equal(ret, 0);

    ret = check_manifest_hash(critical, &sigdata);
    assert_int_equal(ret, 0);

    ret = check_type(critical);
    assert_int_equal(ret, 0);

    amxc_var_delete(&config_data);

    config_data = read_config("bad_signature.json");
    critical = GET_ARG(config_data, "critical");

    ret = check_docker_reference(critical, &sigdata);
    assert_int_not_equal(ret, 0);

    ret = check_manifest_hash(critical, &sigdata);
    assert_int_not_equal(ret, 0);

    ret = check_type(critical);
    assert_int_not_equal(ret, 0);

    amxc_var_delete(&config_data);
    rlyeh_signature_data_clean(&sigdata);
}

void test_retrieve_sigstore_and_creds(UNUSED void** state) {
    int ret;
    amxc_string_t sigstore;
    amxc_string_t username;
    amxc_string_t password;
    amxc_string_init(&sigstore, 0);
    amxc_string_init(&username, 0);
    amxc_string_init(&password, 0);

    ret = retrieve_sigstore_and_creds("sigstore.json", "server", &username, &password, &sigstore);
    assert_int_equal(ret, 0);
    assert_string_equal("username", username.buffer);
    assert_string_equal("password", password.buffer);
    assert_string_equal("server-address", sigstore.buffer);

    ret = retrieve_sigstore_and_creds("sigstore.json", "serverthatdoesnotexist", &username, &password, &sigstore);
    assert_int_not_equal(ret, 0);
    assert_string_equal("username", username.buffer);
    assert_string_equal("password", password.buffer);
    assert_string_equal("server-address", sigstore.buffer);

    amxc_string_clean(&sigstore);
    amxc_string_clean(&username);
    amxc_string_clean(&password);
}

void test_extract_and_verify_signature(UNUSED void** state) {
    int ret;
    char localsigpath[] = "docker_alpine_3_18.signature";
    char* localjsonname = tempnam(RLYEH_TMP_DIR, "sigda");
    rlyeh_signature_data_t sigdata;

    ret = extract_and_verify_signature(localsigpath, localjsonname);
    assert_int_equal(ret, 0);

    rlyeh_signature_data_init(&sigdata);
    sigdata.image_name = strdup("library/alpine");
    sigdata.transport = strdup("https://");
    sigdata.server = strdup("registry-1.docker.io");
    sigdata.version = strdup("3.18.0");
    sigdata.manifest_hash = strdup("c0669ef34cdc14332c0f1ab0c2c01acb91d96014b172f1a76f3a39e63d1f0bda");

    ret = signature_content_verification(localjsonname, &sigdata);
    assert_int_equal(ret, 0);

    rlyeh_signature_data_clean(&sigdata);
    remove(localjsonname);
    free(localjsonname);
}
