/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>

#include <amxc/amxc.h>
#include <rlyeh/rlyeh.h>
#include <rlyeh/rlyeh_image_index_generation.h>
#include <rlyeh_defines_priv.h>

#include <debug/sahtrace.h>

#include "test_rlyeh_image_index_generation.h"

#define UNUSED __attribute__((unused))

static void oops(const char* s1, const char* s2) {
    fprintf(stderr, "Error: %s ", s1);
    perror(s2);
    exit(1);
}

static void copy_file(const char* src, const char* dest, const char* dest_dir) {
#define BUFFERSIZE 128
#define COPYMODE 00755
    int in_fd = 0;
    int out_fd = 0;
    int n_chars = 0;
    char buf[BUFFERSIZE];
    DIR* dir_ptr = NULL;

    if((in_fd = open(src, O_RDONLY)) == -1) {
        oops("Cannot open ", src);
    }
    if(( dir_ptr = opendir(dest_dir)) == NULL) {
        fprintf(stderr, "ls1: cannot open %s\n", dest_dir);
    } else {
        if((out_fd = creat(dest, COPYMODE)) == -1) {
            oops("Cannot creat", dest);
        }
    }
    closedir(dir_ptr);

    while((n_chars = read(in_fd, buf, BUFFERSIZE)) > 0) {
        if(write(out_fd, buf, n_chars) != n_chars) {
            oops("Write error to ", src);
        }
    }
    if(n_chars == -1) {
        oops("Read error from ", src);
    }

    if((close(in_fd) == -1) || (close(out_fd) == -1)) {
        oops("Error closing files", "");
    }
}


static void check_index(const char* filename, rlyeh_index_generation_data_t* data) {
    parser_error err;
    size_t it_manifests;
    image_spec_schema_image_index_schema* index_schema = image_spec_schema_image_index_schema_parse_file(filename, 0, &err);
    size_t it_annotations;
    for(it_manifests = 0; it_manifests < index_schema->manifests_len; it_manifests++) {
        bool found = false;
        for(it_annotations = 0; it_annotations < index_schema->manifests[it_manifests]->annotations->len; it_annotations++) {
            if((strcmp(RLYEH_ANNOTATION_OCI_REFNAME, index_schema->manifests[it_manifests]->annotations->keys[it_annotations]) == 0)
               && (strcmp(data->version, index_schema->manifests[it_manifests]->annotations->values[it_annotations]) == 0)) {
                found = true;
                break;
            }
        }
        if(found) {
            break;
        }
    }
    assert_string_equal(data->manifest_digest, index_schema->manifests[it_manifests]->digest + 7); // <- to remove the "sha256:" prefix
    assert_int_equal(data->manifest_size, index_schema->manifests[it_manifests]->size);
    for(it_annotations = 0; it_annotations < index_schema->manifests[it_manifests]->annotations->len; it_annotations++) {
        if(strcmp(RLYEH_ANNOTATION_SAH_URI, index_schema->manifests[it_manifests]->annotations->keys[it_annotations]) == 0) {
            assert_string_equal(data->uri, index_schema->manifests[it_manifests]->annotations->values[it_annotations]);
        } else if(strcmp(RLYEH_ANNOTATION_SAH_DUID, index_schema->manifests[it_manifests]->annotations->keys[it_annotations]) == 0) {
            assert_string_equal(data->duid, index_schema->manifests[it_manifests]->annotations->values[it_annotations]);
        } else if(strcmp(RLYEH_ANNOTATION_SAH_MAKRFORREMOVAL, index_schema->manifests[it_manifests]->annotations->keys[it_annotations]) == 0) {
            assert_int_equal(data->mark_for_removal, atoi(index_schema->manifests[it_manifests]->annotations->values[it_annotations]));
        }
    }

    free_image_spec_schema_image_index_schema(index_schema);
}

void test_rlyeh_image_index_generate_content(UNUSED void** state) {
    char* index_content;
    amxc_string_t index_filename;
    rlyeh_index_generation_data_t data;
    rlyeh_index_generation_data_init(&data);
    amxc_string_init(&index_filename, 0);
    char* digest = strdup("47046d47361fd624bd80d54e61d4ec7b1a92a06afe2d4b8bded9319d428558b4");
    strcpy(data.manifest_digest, digest);
    data.manifest_size = 100;
    data.version = strdup("version1");
    data.image_name = strdup("/tmp");
    data.uri = strdup("uri");
    data.duid = strdup("duid");
    data.mark_for_removal = false;
    amxc_string_setf(&index_filename, "%s/index.json", data.image_name);

    if(file_exists(index_filename.buffer)) {
        remove(index_filename.buffer);
    }
    index_content = rlyeh_image_index_generate_content(&data);
    if(index_content) {
        rlyeh_write_index(index_filename.buffer, index_content);
    }
    free(index_content);
    check_index(index_filename.buffer, &data);

    // Test update the index
    data.mark_for_removal = true;
    index_content = rlyeh_image_index_generate_content(&data);
    if(index_content) {
        rlyeh_write_index(index_filename.buffer, index_content);
    }
    free(index_content);
    check_index(index_filename.buffer, &data);

    free(data.version);
    data.version = strdup("version2");
    index_content = rlyeh_image_index_generate_content(&data);
    if(index_content) {
        rlyeh_write_index(index_filename.buffer, index_content);
    }
    free(index_content);
    check_index(index_filename.buffer, &data);

    remove(index_filename.buffer);

    // Clean data
    free(data.version);
    free(data.image_name);
    free(data.uri);
    free(data.duid);
    free(digest);
    amxc_string_clean(&index_filename);

}

void test_rlyeh_update_index_annotations(UNUSED void** state) {
    rlyeh_index_generation_data_t data;
    rlyeh_index_generation_data_init(&data);
    char* digest = strdup("fbaa26ed00e83b33bf2c7b6abe3ce24cf74da3b0cde3e6e44215aa1d3e0f1e46");
    strcpy(data.manifest_digest, digest);
    data.manifest_size = 1;
    data.version = strdup("latest");
    data.image_name = strdup("images/image1");
    data.uri = strdup("uri");
    data.duid = strdup("duid");
    data.mark_for_removal = true;

    copy_file("images/image1/index.json.orig", "images/image1/index.json", "images/image1");

    rlyeh_update_index_annotations("images", "image1", "duid", "latest", "", "1");
    check_index("images/image1/index.json", &data);

    rlyeh_update_index_annotations("images", "image2", "duid", "latest", "", "0");
    check_index("images/image1/index.json", &data);

    remove("images/image1/index.json");

    // Clean data
    free(data.version);
    free(data.image_name);
    free(data.uri);
    free(data.duid);
    free(digest);
}