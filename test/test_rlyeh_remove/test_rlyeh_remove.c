/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>

#include <amxc/amxc.h>
#include <debug/sahtrace.h>

#include <rlyeh/rlyeh.h>
#include <rlyeh/rlyeh_remove.h>
#include "test_rlyeh_remove.h"

#define UNUSED __attribute__((unused))

static void oops(const char* s1, const char* s2) {
    fprintf(stderr, "Error: %s ", s1);
    perror(s2);
    exit(1);
}


static void copy_file(const char* src, const char* dest, const char* dest_dir) {
#define BUFFERSIZE 128
#define COPYMODE 00755
    int in_fd = 0;
    int out_fd = 0;
    int n_chars = 0;
    char buf[BUFFERSIZE];
    DIR* dir_ptr;

    if((in_fd = open(src, O_RDONLY)) == -1) {
        oops("Cannot open ", src);
    }
    if(( dir_ptr = opendir(dest_dir)) == NULL) {
        fprintf(stderr, "ls1: cannot open %s\n", dest_dir);
    } else {
        if((out_fd = creat(dest, COPYMODE)) == -1) {
            oops("Cannot creat", dest);
        }
    }
    closedir(dir_ptr);

    while((n_chars = read(in_fd, buf, BUFFERSIZE)) > 0) {
        if(write(out_fd, buf, n_chars) != n_chars) {
            oops("Write error to ", src);
        }
    }
    if(n_chars == -1) {
        oops("Read error from ", src);
    }

    if((close(in_fd) == -1) || (close(out_fd) == -1)) {
        oops("Error closing files", "");
    }
}

static void copy_dir_content(const char* src, const char* dest) {
    DIR* dir;
    struct dirent* direntp;

    if(( dir = opendir(src)) == NULL) {
        fprintf(stderr, "ls1: cannot open %s\n", src);
    } else {
        while(( direntp = readdir(dir)) != NULL) {
            if((strcmp(direntp->d_name, ".") == 0) || (strcmp(direntp->d_name, "..") == 0)) {
                continue;
            }
            amxc_string_t src_fname;
            amxc_string_t dest_fname;
            amxc_string_init(&src_fname, 0);
            amxc_string_init(&dest_fname, 0);
            amxc_string_setf(&src_fname, "%s/%s", src, direntp->d_name);
            amxc_string_setf(&dest_fname, "%s/%s", dest, direntp->d_name);
            copy_file(src_fname.buffer, dest_fname.buffer, dest);
            amxc_string_clean(&src_fname);
            amxc_string_clean(&dest_fname);
        }
        closedir(dir);
    }
}

void test_rlyeh_remove(UNUSED void** state) {
    rlyeh_remove_data_t data;
    rlyeh_remove_data_init(&data);

    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_remove");
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, "Generic");
    sahTraceOpen("RLYEH_TEST", TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO("Generic", "sahTrace initialized");

    // set remove env in /tmp/test_rlyeh/
    mkdir("/tmp/test_rlyeh", 0755);
    mkdir("/tmp/test_rlyeh/images", 0755);
    mkdir("/tmp/test_rlyeh/images/image1", 0755);
    mkdir("/tmp/test_rlyeh/images/image2", 0755);

    mkdir("/tmp/test_rlyeh/blobs", 0755);
    mkdir("/tmp/test_rlyeh/blobs/sha256", 0755);

    copy_dir_content("images/image1", "/tmp/test_rlyeh/images/image1");
    copy_dir_content("images/image2", "/tmp/test_rlyeh/images/image2");
    copy_dir_content("blobs/sha256", "/tmp/test_rlyeh/blobs/sha256");

    data.imageLocation = strdup("/tmp/test_rlyeh/images");
    data.storageLocation = strdup("/tmp/test_rlyeh/blobs");
    data.name = strdup("image1");
    data.version = strdup("latest");
    rlyeh_remove(&data);

    assert_true(file_exists("/tmp/test_rlyeh/images/image1/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh/images/image1/oci-layout"));
    assert_false(file_exists("/tmp/test_rlyeh/blobs/sha256/blob1"));
    assert_true(file_exists("/tmp/test_rlyeh/blobs/sha256/blob3")); // <-- shared blob
    assert_true(file_exists("/tmp/test_rlyeh/blobs/sha256/blob4")); // <-- shared blob
    assert_true(file_exists("/tmp/test_rlyeh/blobs/sha256/blob7")); // <-- shared blob

    rlyeh_remove_data_clean(&data);
    rlyeh_remove_data_init(&data);
    data.imageLocation = strdup("/tmp/test_rlyeh/images");
    data.storageLocation = strdup("/tmp/test_rlyeh/blobs");
    data.name = strdup("image2");
    data.version = strdup("latest");
    rlyeh_remove(&data);

    assert_false(file_exists("/tmp/test_rlyeh/images/image2/index.json"));
    assert_false(file_exists("/tmp/test_rlyeh/images/image2/oci-layout"));
    assert_false(file_exists("/tmp/test_rlyeh/blobs/sha256/blob2"));
    assert_false(file_exists("/tmp/test_rlyeh/blobs/sha256/blob5"));
    assert_true(file_exists("/tmp/test_rlyeh/blobs/sha256/blob6"));  // <-- shared blob
    assert_false(file_exists("/tmp/test_rlyeh/blobs/sha256/blob7")); // <-- shared blob

    rlyeh_remove_data_clean(&data);
    rlyeh_remove_data_init(&data);
    data.imageLocation = strdup("/tmp/test_rlyeh/images");
    data.storageLocation = strdup("/tmp/test_rlyeh/blobs");
    data.name = strdup("image1");
    data.version = strdup("v1");
    rlyeh_remove(&data);

    assert_false(file_exists("/tmp/test_rlyeh/images/image1/index.json"));
    assert_false(file_exists("/tmp/test_rlyeh/images/image1/oci-layout"));
    assert_false(file_exists("/tmp/test_rlyeh/blobs/sha256/blob2"));
    assert_false(file_exists("/tmp/test_rlyeh/blobs/sha256/blob3"));
    assert_false(file_exists("/tmp/test_rlyeh/blobs/sha256/blob4")); // <-- shared blob
    assert_false(file_exists("/tmp/test_rlyeh/blobs/sha256/blob6")); // <-- shared blob

    assert_false(dir_exists("/tmp/test_rlyeh/blobs/sha256"));


    rmdir("/tmp/test_rlyeh/images");
    rmdir("/tmp/test_rlyeh/blobs");
    rmdir("/tmp/test_rlyeh");

    rlyeh_remove_data_clean(&data);
    sahTraceClose();

}

void test_rlyeh_remove_unlisted_blobs(UNUSED void** state) {
    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_remove");
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, "Generic");
    sahTraceOpen("RLYEH_TEST", TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO("Generic", "sahTrace initialized");

    // set remove env in /tmp/test_rlyeh/
    mkdir("/tmp/test_rlyeh", 0755);
    mkdir("/tmp/test_rlyeh/images", 0755);
    mkdir("/tmp/test_rlyeh/images/image1", 0755);

    mkdir("/tmp/test_rlyeh/blobs", 0755);
    mkdir("/tmp/test_rlyeh/blobs/sha256", 0755);

    copy_dir_content("images/image1", "/tmp/test_rlyeh/images/image1");
    copy_dir_content("blobs/sha256", "/tmp/test_rlyeh/blobs/sha256");

    rlyeh_remove_unlisted_blobs("/tmp/test_rlyeh/blobs", "/tmp/test_rlyeh/images");
    assert_false(file_exists("/tmp/test_rlyeh/blobs/sha256/blob2"));
    assert_false(file_exists("/tmp/test_rlyeh/blobs/sha256/blob5"));

    rmdir("/tmp/test_rlyeh/images");
    rmdir("/tmp/test_rlyeh/blobs");
    rmdir("/tmp/test_rlyeh");

    sahTraceClose();
}

void test_rlyeh_remove_unlisted_imagespec(UNUSED void** state) {
    amxc_llist_t llist;
    amxc_llist_init(&llist);
    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_remove");
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, "Generic");
    sahTraceOpen("RLYEH_TEST", TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO("Generic", "sahTrace initialized");

    // set remove env in /tmp/test_rlyeh/
    mkdir("/tmp/test_rlyeh", 0755);
    mkdir("/tmp/test_rlyeh/images", 0755);
    mkdir("/tmp/test_rlyeh/images/image1", 0755);
    mkdir("/tmp/test_rlyeh/images/image2", 0755);
    // mkdir("/tmp/test_rlyeh/images/image3", 0755);

    mkdir("/tmp/test_rlyeh/blobs", 0755);
    mkdir("/tmp/test_rlyeh/blobs/sha256", 0755);

    copy_dir_content("images/image1", "/tmp/test_rlyeh/images/image1");
    copy_dir_content("images/image2", "/tmp/test_rlyeh/images/image2");
    copy_dir_content("blobs/sha256", "/tmp/test_rlyeh/blobs/sha256");

    amxc_llist_add_string(&llist, "image1:v1");

    rlyeh_remove_unlisted_imagespec("/tmp/test_rlyeh/blobs", "/tmp/test_rlyeh/images", &llist);
    assert_true(file_exists("/tmp/test_rlyeh/images/image1/index.json"));
    assert_true(file_exists("/tmp/test_rlyeh/images/image1/oci-layout"));
    assert_false(file_exists("/tmp/test_rlyeh/images/image2/index.json"));
    assert_false(file_exists("/tmp/test_rlyeh/images/image2/oci-layout"));
    assert_false(file_exists("/tmp/test_rlyeh/blobs/sha256/blob1"));
    assert_false(file_exists("/tmp/test_rlyeh/blobs/sha256/blob2"));
    assert_false(file_exists("/tmp/test_rlyeh/blobs/sha256/blob5"));
    assert_false(file_exists("/tmp/test_rlyeh/blobs/sha256/blob7"));

    rmdir("/tmp/test_rlyeh/images");
    rmdir("/tmp/test_rlyeh/blobs");
    rmdir("/tmp/test_rlyeh");

    sahTraceClose();
    amxc_llist_clean(&llist, amxc_string_list_it_free);
}

void test_rlyeh_remove_invalid_imagespec(UNUSED void** state) {
    amxc_llist_t llist;
    amxc_llist_init(&llist);
    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_remove");
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, "Generic");
    sahTraceOpen("RLYEH_TEST", TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO("Generic", "sahTrace initialized");

    // set remove env in /tmp/test_rlyeh/
    mkdir("/tmp/test_rlyeh", 0755);
    mkdir("/tmp/test_rlyeh/images", 0755);
    mkdir("/tmp/test_rlyeh/images/invalid", 0755);

    copy_dir_content("images/invalid", "/tmp/test_rlyeh/images/invalid");

    amxc_llist_add_string(&llist, "invalid:v1");

    rlyeh_remove_unlisted_imagespec("/tmp/test_rlyeh/blobs", "/tmp/test_rlyeh/images", &llist);
    assert_false(file_exists("/tmp/test_rlyeh/images/invalid/index.json"));

    rmdir("/tmp/test_rlyeh/images");
    rmdir("/tmp/test_rlyeh/blobs");
    rmdir("/tmp/test_rlyeh");

    sahTraceClose();
    amxc_llist_clean(&llist, amxc_string_list_it_free);
}