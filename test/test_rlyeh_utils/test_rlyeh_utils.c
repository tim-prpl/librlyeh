/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <fcntl.h>
#include <unistd.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <rlyeh/rlyeh.h>

#include "test_rlyeh_utils.h"

#define UNUSED __attribute__((unused))

void test_rlyeh_parse_uri(UNUSED void** state) {
    rlyeh_image_parameters_t uri_elements;

    rlyeh_image_parameters_init(&uri_elements);
    rlyeh_parse_uri("transport://image", &uri_elements);
    assert_string_equal(uri_elements.transport, "transport");
    assert_string_equal(uri_elements.image_name, "image");
    assert_true(amxc_string_is_empty(&uri_elements.server));
    assert_string_equal(uri_elements.version, "latest");
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    rlyeh_parse_uri("transport://server/alpine", &uri_elements);
    assert_string_equal(uri_elements.transport, "transport");
    assert_string_equal(uri_elements.server.buffer, "server");
    assert_string_equal(uri_elements.image_name, "alpine");
    assert_string_equal(uri_elements.version, "latest");
    rlyeh_image_parameters_clean(&uri_elements);

    rlyeh_image_parameters_init(&uri_elements);
    rlyeh_parse_uri("transport://server/image_name:version", &uri_elements);
    assert_string_equal(uri_elements.transport, "transport");
    assert_string_equal(uri_elements.server.buffer, "server");
    assert_string_equal(uri_elements.image_name, "image_name");
    assert_string_equal(uri_elements.version, "version");
    rlyeh_image_parameters_clean(&uri_elements);


    rlyeh_image_parameters_init(&uri_elements);
    rlyeh_parse_uri("docker://docker.v3d.fr/embedded-gui-hgw-generic-webui:1.2.2", &uri_elements);
    assert_string_equal(uri_elements.transport, "docker");
    assert_string_equal(uri_elements.server.buffer, "docker.v3d.fr");
    assert_string_equal(uri_elements.image_name, "embedded-gui-hgw-generic-webui");
    assert_string_equal(uri_elements.version, "1.2.2");
    rlyeh_image_parameters_clean(&uri_elements);

}

void test_rlyeh_parse_local_image(UNUSED void** state) {

    rlyeh_image_parameters_t param;
    rlyeh_image_parameters_init(&param);

    rlyeh_parse_local_image("part1:part2:part3", &param);
    assert_string_equal(param.transport, "part1");
    assert_string_equal(param.image_name, "part2");
    assert_string_equal(param.version, "part3");

    rlyeh_image_parameters_clean(&param);
}

void test_build_manifest_filename_from_index(UNUSED void** state) {
    bool ret = false;

    parser_error err = NULL;
    amxc_string_t fname;
    amxc_string_init(&fname, 0);
    image_spec_schema_image_index_schema* index_schema = NULL;

    index_schema = image_spec_schema_image_index_schema_parse_file("index.json", 0, &err);
    ret = build_manifest_filename_from_index(index_schema, "latest", "blobs", &fname);
    assert_false(ret);
    assert_string_equal("blobs/sha256/47046d47361fd624bd80d54e61d4ec7b1a92a06afe2d4b8bded9319d428558b4", fname.buffer);
    free_image_spec_schema_image_index_schema(index_schema);
    amxc_string_clean(&fname);

    index_schema = image_spec_schema_image_index_schema_parse_file("bad_index.json", 0, &err);
    ret = build_manifest_filename_from_index(index_schema, "latest", "blobs", &fname);
    assert_true(ret);
    assert_true(amxc_string_is_empty(&fname));
    free_image_spec_schema_image_index_schema(index_schema);
    amxc_string_clean(&fname);
}

void test_check_manifest_validity(UNUSED void** state) {
    image_spec_schema_image_manifest_schema* image_manifest = NULL;
    parser_error err = NULL;
    bool ret = false;
    image_manifest = image_spec_schema_image_manifest_schema_parse_file("manifest.json", NULL, &err);
    ret = check_manifest_validity(image_manifest, err);
    assert_false(ret);
    free_image_spec_schema_image_manifest_schema(image_manifest);

    image_manifest = image_spec_schema_image_manifest_schema_parse_file("bad_manifest.json", NULL, &err);
    ret = check_manifest_validity(image_manifest, err);
    assert_true(ret);
    free_image_spec_schema_image_manifest_schema(image_manifest);

    ret = check_manifest_validity(NULL, err);
    assert_true(ret);
}

void test_image_already_exists(UNUSED void** state) {
    bool ret = false;
    rlyeh_image_parameters_t param;
    rlyeh_image_parameters_init(&param);

    ret = image_already_exists(&param);
    assert_false(ret);

    param.image_name = strdup("dir");
    param.version = strdup("latest");
    ret = image_already_exists(&param);
    assert_false(ret);

    free(param.image_name);
    param.image_name = strdup("..");
    ret = image_already_exists(&param);
    assert_false(ret);

    free(param.image_name);
    param.image_name = strdup(".");
    ret = image_already_exists(&param);
    assert_true(ret);

    rlyeh_image_parameters_clean(&param);
    rlyeh_image_parameters_clean(NULL);
}

void test_rlyeh_get_creds_from_cmd(UNUSED void** state) {
    rlyeh_image_parameters_t param;
    char* creds = strdup("usr:pwd");

    rlyeh_image_parameters_init(&param);
    rlyeh_get_creds_from_cmd(creds, &param);
    assert_string_equal(param.username.buffer, "usr");
    assert_string_equal(param.password.buffer, "pwd");
    rlyeh_image_parameters_clean(&param);
    free(creds);
}

void test_shorten_image_name(UNUSED void** state) {

    char* shorten = NULL;

    shorten = shorten_image_name("one");
    assert_string_equal(shorten, "one");
    free(shorten);
    shorten = shorten_image_name("one/two");
    assert_string_equal(shorten, "one/two");
    free(shorten);
    shorten = shorten_image_name("one/two/three");
    assert_string_equal(shorten, "one/two/three");
    free(shorten);
    shorten = shorten_image_name("longlonglong/two/threethreethreethreethreethreethreethreethreethree");
    assert_string_equal(shorten, "two/threethreethreethreethreethreethreethreethreethree");
    free(shorten);
    // 64 chars
    shorten = shorten_image_name("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    assert_string_equal(shorten, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    free(shorten);
    // 65 chars
    shorten = shorten_image_name("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    assert_string_equal(shorten, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    free(shorten);
    // 64 chars, last a slash
    shorten = shorten_image_name("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/");
    assert_string_equal(shorten, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/");
    free(shorten);
    // 65 chars, last a slash
    shorten = shorten_image_name("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/");
    assert_string_equal(shorten, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    free(shorten);
}

void test_file_is_symlink(UNUSED void** state) {
    char* filename = tempnam("/tmp/", "rlyeh");
    char* link;
    int ret;
    bool ret2;

    int fd = open(filename, O_WRONLY | O_CREAT);
    assert_true(fd >= 0);
    close(fd);

    link = tempnam("/tmp/", "rlyeh");
    ret = symlink(filename, link);
    assert_int_equal(ret, 0);

    ret2 = file_is_symlink(filename);
    assert_false(ret2);

    ret2 = file_is_symlink(link);
    assert_true(ret2);

    ret2 = file_is_symlink(NULL);
    assert_false(ret2);

    ret2 = file_is_symlink("");
    assert_false(ret2);

    ret2 = file_is_symlink("/imaginaryfile/inamysteriousfolder");
    assert_false(ret2);

    free(filename);
    free(link);
}

void test_file_exists(UNUSED void** state) {
    char* filename = tempnam("/tmp/", "rlyeh");
    bool ret2;

    ret2 = file_exists(filename);
    assert_false(ret2);

    int fd = open(filename, O_WRONLY | O_CREAT);
    assert_true(fd >= 0);
    close(fd);

    ret2 = file_exists(filename);
    assert_true(ret2);

    ret2 = file_exists(NULL);
    assert_false(ret2);

    ret2 = file_exists("");
    assert_false(ret2);

    ret2 = file_exists("/");
    assert_true(ret2);

    free(filename);
}

void test_dir_exists(UNUSED void** state) {
    char* dirname = tempnam("/tmp/", "rlyeh");
    char* filename = tempnam("/tmp/", "rlyeh");
    bool ret2;

    ret2 = dir_exists(dirname);
    assert_false(ret2);

    ret2 = dir_exists("/");
    assert_true(ret2);

    int fd = open(filename, O_WRONLY | O_CREAT);
    assert_true(fd >= 0);
    close(fd);

    ret2 = dir_exists(filename);
    assert_false(ret2);

    ret2 = dir_exists(NULL);
    assert_false(ret2);

    ret2 = dir_exists("");
    assert_false(ret2);

    free(dirname);
    free(filename);
}

#define SHA256 "sha256"
#define PATH "BLOB"

void test_digest_to_path(UNUSED void** state) {
    const char* digest1 = SHA256 ":" PATH;
    const char* digest2 = PATH;
    char* path = NULL;

    path = digest_to_path(digest1);
    assert_true(path != NULL);
    assert_true(strcmp(path, SHA256 "/" PATH) == 0);
    free(path);

    path = digest_to_path(digest2);
    assert_true(path != NULL);
    assert_true(strcmp(path, PATH) == 0);
    free(path);

    path = digest_to_path(NULL);
    assert_true(path == NULL);

    path = digest_to_path("");
    assert_true(path == NULL);
}
