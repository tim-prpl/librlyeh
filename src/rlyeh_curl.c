/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include <rlyeh_priv.h>
#include <rlyeh_assert.h>
#include <debug/sahtrace.h>

#include <rlyeh/rlyeh_curl.h>
#include <rlyeh/rlyeh_status.h>
#include <rlyeh/rlyeh_utils.h>

#define ME "rlyeh_copy"

static CURL* curl = NULL;

static inline int apply_curl_certificate_verification(bool enable) {
    if(enable) {
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2L);
    } else {
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
    }
    return 0;
}

static size_t write_curl_call_back_in_char(void* data, size_t size, size_t nmemb, void* ctx) {
    static size_t sz = 0;
    size_t currsz = size * nmemb;

    // Reset sz between two Rlyeh.pull()
    if(*(char**) ctx == NULL) {
        sz = 0;
    }

    size_t prevsz = sz;
    sz += currsz;
    void* tmp = realloc(*(char**) ctx, sz);
    if(tmp == NULL) {
        // handle error
        free(*(char**) ctx);
        *(char**) ctx = NULL;
        return 0;
    }
    *(char**) ctx = (char*) tmp;

    // The last line of the token causes memory leaks. Its original content is "\r\n\0"
    // Overwritting the last line to "/0" solves the problem.
    if(strcmp((char*) data, "\r\n\0") == 0) {
        memcpy(*(char**) ctx + prevsz, (char*) data + 2, currsz);
    } else {
        memcpy(*(char**) ctx + prevsz, data, currsz);
    }
    return currsz;
}

static size_t write_curl_call_back_in_file(void* contents, size_t size, size_t nmemb, void* userp) {
    FILE* fd = (FILE*) userp;
    size_t ret = fwrite(contents, size, nmemb, fd);
    if(ret < (size * nmemb)) {
        SAH_TRACEZ_ERROR(ME, "Cannot write to file (%d) : %s", errno, strerror(errno));
    }

    return ret;
}

static rlyeh_status_t map_curlcode_to_rlyehstatus(CURLcode value) {
    rlyeh_status_t status = RLYEH_NO_ERROR;
    switch(value) {
    case CURLE_OK:
        status = RLYEH_NO_ERROR;
        break;
    case CURLE_OPERATION_TIMEDOUT:
        status = RLYEH_ERROR_CURL_OPERATION_TIMEDOUT;
        break;
    case CURLE_COULDNT_RESOLVE_HOST:
        status = RLYEH_ERROR_CURL_COULDNT_RESOLVE_HOST;
        break;
    case CURLE_WRITE_ERROR:
        status = RLYEH_ERROR_DISK_SPACE;
        break;
    case CURLE_SSL_CERTPROBLEM:
    case CURLE_PEER_FAILED_VERIFICATION:
        status = RLYEH_ERROR_CERTIFICATE_CHECK_FAILED;
        break;
    default:
        status = RLYEH_ERROR_DOWNLOAD_FAILED;
        break;
    }
    return status;
}

rlyeh_status_t rlyeh_curl_download_content(const rlyeh_curl_parameters_t* parameters, const amxc_string_t* url, void* data, curl_off_t* size, char* err_msg) {
    CURLcode res = CURLE_OK;
    struct curl_slist* list = NULL;
    rlyeh_status_t status = RLYEH_NO_ERROR;
    char curl_err_str[CURL_ERROR_SIZE];
    amxc_string_t opt_token;

    if(!curl) {
        SAH_TRACEZ_ERROR(ME, "Call rlyeh_curl_init before calling this function");
        return RLYEH_ERROR_UNKNOWN_ERROR;
    }
    curl_easy_reset(curl);

    if(parameters->type == FILE_TYPE_MANIFEST) {
        list = curl_slist_append(list, "Accept: application/vnd.oci.image.manifest.v1+json");
        list = curl_slist_append(list, "Accept: application/vnd.docker.distribution.manifest.v2+json");
    }

    switch(parameters->auth_type) {
    case BASIC_AUTHENTICATION:
        curl_easy_setopt(curl, CURLOPT_USERNAME, parameters->username->buffer);
        curl_easy_setopt(curl, CURLOPT_PASSWORD, parameters->password->buffer);
        break;
    case BEARER_AUTHENTICATION:
        amxc_string_init(&opt_token, 0);
        amxc_string_setf(&opt_token, "Authorization: Bearer %s", parameters->token->buffer);
        list = curl_slist_append(list, opt_token.buffer);
        amxc_string_clean(&opt_token);
        break;
    case NO_AUTHENTICATION:
    default:
        SAH_TRACEZ_WARNING(ME, "Downloading with NO_AUTHENTICATION");
        break;
    }

    curl_easy_setopt(curl, CURLOPT_URL, url->buffer);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, data);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, (void*) write_curl_call_back_in_file);
    // follow redirection
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    /* abort if slower than 30 bytes/sec during 10 seconds */
    curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 10L);
    curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 30L);

    curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1L);
    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, curl_err_str);

    apply_curl_certificate_verification(parameters->cv);

    SAH_TRACEZ_INFO(ME, "curl_easy_perform url: [%s]", url->buffer);
    res = curl_easy_perform(curl);
    switch(res) {
    case CURLE_OK:
        curl_easy_getinfo(curl, CURLINFO_SIZE_DOWNLOAD_T, size);
        break;
    case CURLE_WRITE_ERROR:
        set_err_msg(err_msg, "Curl download content error [%s] URL [%s] - cannot write to file - disk full?", curl_err_str, url->buffer);
        SAH_TRACEZ_ERROR(ME, "curl_easy_perform() failed: %s - cannot write to file - disk full?", curl_err_str);
        break;
    default:
        set_err_msg(err_msg, "Curl download content error [%s] URL [%s]", curl_err_str, url->buffer);
        SAH_TRACEZ_ERROR(ME, "curl_easy_perform() failed: %s", curl_err_str);
        break;
    }
    status = map_curlcode_to_rlyehstatus(res);

    curl_slist_free_all(list);

    return status;
}

rlyeh_status_t rlyeh_curl_head(const rlyeh_curl_parameters_t* parameters, const amxc_string_t* url, void* data, curl_off_t* size, char* err_msg) {
    CURLcode res = CURLE_OK;
    struct curl_slist* list = NULL;
    rlyeh_status_t status = RLYEH_NO_ERROR;
    char curl_err_str[CURL_ERROR_SIZE];
    amxc_string_t opt_token;

    if(!curl) {
        SAH_TRACEZ_ERROR(ME, "Call rlyeh_curl_init before calling this function");
        return RLYEH_ERROR_UNKNOWN_ERROR;
    }
    curl_easy_reset(curl);

    if(parameters->type == FILE_TYPE_MANIFEST) {
        list = curl_slist_append(list, "Accept: application/vnd.oci.image.manifest.v1+json");
        list = curl_slist_append(list, "Accept: application/vnd.docker.distribution.manifest.v2+json");
    }


    switch(parameters->auth_type) {
    case BASIC_AUTHENTICATION:
        curl_easy_setopt(curl, CURLOPT_USERNAME, parameters->username->buffer);
        curl_easy_setopt(curl, CURLOPT_PASSWORD, parameters->password->buffer);
        break;
    case BEARER_AUTHENTICATION:
        amxc_string_init(&opt_token, 0);
        amxc_string_setf(&opt_token, "Authorization: Bearer %s", parameters->token->buffer);
        list = curl_slist_append(list, opt_token.buffer);
        amxc_string_clean(&opt_token);
        break;
    case NO_AUTHENTICATION:
    default:
        break;
    }

    curl_easy_setopt(curl, CURLOPT_URL, url->buffer);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
    curl_easy_setopt(curl, CURLOPT_HEADER, 1L);
    curl_easy_setopt(curl, CURLOPT_NOBODY, 1L);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, data);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, (void*) write_curl_call_back_in_file);
    // follow redirection
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    /* abort if slower than 30 bytes/sec during 10 seconds */
    curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 10L);
    curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 30L);

    curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1L);
    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, curl_err_str);

    apply_curl_certificate_verification(parameters->cv);

    SAH_TRACEZ_INFO(ME, "curl_easy_perform url: [%s]", url->buffer);
    // fprintf(stderr, "curl_easy_perform url: [%s]", url->buffer);
    res = curl_easy_perform(curl);
    switch(res) {
    case CURLE_OK:
        curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD_T, size);
        break;
    case CURLE_WRITE_ERROR:
        set_err_msg(err_msg, "Curl download content error [%s] URL [%s] - cannot write to file - disk full?", curl_err_str, url->buffer);
        SAH_TRACEZ_ERROR(ME, "curl_easy_perform() failed: %s - cannot write to file - disk full?", curl_err_str);
        break;
    default:
        set_err_msg(err_msg, "Curl download content error [%s] URL [%s]", curl_err_str, url->buffer);
        SAH_TRACEZ_ERROR(ME, "curl_easy_perform() failed: %s", curl_err_str);
        break;
    }
    status = map_curlcode_to_rlyehstatus(res);


    curl_slist_free_all(list);

    return status;
}

rlyeh_status_t rlyeh_curl_get_httpauthentication(const amxc_string_t* url, void* buf, long* auth, char* err_msg, bool cv) {
    CURLcode res = CURLE_OK;
    rlyeh_status_t status = RLYEH_NO_ERROR;
    char curl_err_str[CURL_ERROR_SIZE];

    if(!curl) {
        SAH_TRACEZ_ERROR(ME, "Call rlyeh_curl_init before calling this function");
        return RLYEH_ERROR_UNKNOWN_ERROR;
    }
    curl_easy_reset(curl);

    curl_easy_setopt(curl, CURLOPT_URL, url->buffer);
    curl_easy_setopt(curl, CURLOPT_HEADER, 1L);
    curl_easy_setopt(curl, CURLOPT_NOBODY, 1L);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, buf);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, (void*) write_curl_call_back_in_char);
    // follow redirection
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    /* abort if slower than 30 bytes/sec during 10 seconds */
    curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 10L);
    curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 30L);
    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, curl_err_str);
    apply_curl_certificate_verification(cv);

    SAH_TRACEZ_INFO(ME, "curl_easy_perform url: [%s]", url->buffer);
    res = curl_easy_perform(curl);
    switch(res) {
    case CURLE_OK:
        curl_easy_getinfo(curl, CURLINFO_HTTPAUTH_AVAIL, auth);
        break;
    case CURLE_WRITE_ERROR:
        set_err_msg(err_msg, "Curl download content error [%s] URL [%s] - cannot write to file - disk full?", curl_err_str, url->buffer);
        SAH_TRACEZ_ERROR(ME, "curl_easy_perform() failed: %s - cannot write to file - disk full?", curl_err_str);
        break;
    default:
        set_err_msg(err_msg, "Curl download content error [%s] URL [%s]", curl_err_str, url->buffer);
        SAH_TRACEZ_ERROR(ME, "curl_easy_perform() failed: %s", curl_err_str);
        break;
    }

    status = map_curlcode_to_rlyehstatus(res);

    return status;
}

rlyeh_status_t rlyeh_curl_get(const amxc_string_t* url, rlyeh_image_parameters_t* src, void* data, char* err_msg, bool cv) {
    CURLcode res = CURLE_OK;
    rlyeh_status_t status = RLYEH_NO_ERROR;
    char curl_err_str[CURL_ERROR_SIZE];

    if(!curl) {
        SAH_TRACEZ_ERROR(ME, "Call rlyeh_curl_init before calling this function");
        return RLYEH_ERROR_UNKNOWN_ERROR;
    }
    curl_easy_reset(curl);

    if(!amxc_string_is_empty(&src->username) && !amxc_string_is_empty(&src->password)) {
        curl_easy_setopt(curl, CURLOPT_USERNAME, amxc_string_get(&src->username, 0));
        curl_easy_setopt(curl, CURLOPT_PASSWORD, amxc_string_get(&src->password, 0));
    }

    curl_easy_setopt(curl, CURLOPT_URL, url->buffer);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, data);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, (void*) write_curl_call_back_in_file);
    // follow redirection
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    /* abort if slower than 30 bytes/sec during 10 seconds */
    curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 10L);
    curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 30L);
    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, curl_err_str);
    curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1L);

    apply_curl_certificate_verification(cv);

    SAH_TRACEZ_INFO(ME, "curl_easy_perform url: [%s]", url->buffer);
    res = curl_easy_perform(curl);
    switch(res) {
    case CURLE_WRITE_ERROR:
        set_err_msg(err_msg, "Curl download content error [%s] URL [%s] - cannot write to file - disk full?", curl_err_str, url->buffer);
        SAH_TRACEZ_ERROR(ME, "curl_easy_perform() failed: %s - cannot write to file - disk full?", curl_err_str);
        break;
    default:
        set_err_msg(err_msg, "Curl download content error [%s] URL [%s]", curl_err_str, url->buffer);
        SAH_TRACEZ_ERROR(ME, "curl_easy_perform() failed: %s", curl_err_str);
        break;
    }
    status = map_curlcode_to_rlyehstatus(res);

    return status;
}

int rlyeh_curl_init(void) {
    if(curl) {
        SAH_TRACEZ_ERROR(ME, "CURL is already initialized");
        return -1;
    }
    curl_global_init(CURL_GLOBAL_ALL);
    curl = curl_easy_init();
    return 0;
}

void rlyeh_curl_cleanup(void) {
    if(curl) {
        curl_easy_cleanup(curl);
        curl_global_cleanup();
    }
    curl = NULL;
}
