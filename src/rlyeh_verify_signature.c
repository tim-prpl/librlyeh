/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <locale.h>

#include <gpgme.h>

#include <yajl/yajl_gen.h>
#include <amxc/amxc.h>
#include <amxj/amxj_variant.h>

#include <rlyeh/rlyeh_utils.h>
#include <rlyeh/rlyeh_curl.h>
#include <rlyeh/rlyeh_verify_signature.h>
#include <rlyeh_verify_signature_priv.h>
#include <rlyeh_priv.h>
#include <rlyeh_assert.h>
#include <rlyeh_defines_priv.h>

#include <debug/sahtrace.h>

#define ME "rlyeh_signature"

#define OCI_SIGNATURE_DOCKER_REFERENCE "docker-reference"
#define OCI_SIGNATURE_DOCKER_MANIFEST_DIGEST "docker-manifest-digest"
#define OCI_SIGNATURE_TYPE "type"

#define print_err(err)                    \
    do                                \
    {                               \
        if(err)                          \
        {                           \
            SAH_TRACEZ_ERROR(ME, "%s:%d: %s: %s\n",           \
                             __FILE__, __LINE__, gpgme_strsource(err),   \
                             gpgme_strerror(err));           \
        }                           \
    }                               \
    while(0)

static int init_gpgme(gpgme_protocol_t proto) {
    int ret = -1;
    const char* version;
    gpgme_error_t err;
    /* Initialize the locale environment.  */
    setlocale(LC_ALL, "C.UTF-8");
    version = gpgme_check_version(NULL);
    if(version) {
        SAH_TRACEZ_INFO(ME, "GPGME version '%s'", version);
    } else {
        SAH_TRACEZ_ERROR(ME, "GPGME does not return a version");
        goto exit;
    }

    gpgme_set_locale(NULL, LC_CTYPE, setlocale(LC_CTYPE, "C.UTF-8"));

    err = gpgme_engine_check_version(proto);
    print_err(err);

    ret = 0;
exit:
    return ret;
}

static void print_data(gpgme_data_t data, FILE* fd) {
#define BUF_SIZE 512
    char buf[BUF_SIZE + 1];
    int ret;

    ret = gpgme_data_seek(data, 0, SEEK_SET);
    if(ret) {
        print_err(gpgme_err_code_from_errno(errno));
    }
    while((ret = gpgme_data_read(data, buf, BUF_SIZE)) > 0) {
        fwrite(buf, ret, 1, fd);
    }
    if(ret < 0) {
        print_err(gpgme_err_code_from_errno(errno));
    }

    /* Reset read position to the beginning */
    ret = gpgme_data_seek(data, 0, SEEK_SET);
    if(ret) {
        print_err(gpgme_err_code_from_errno(errno));
    }
}

int retrieve_sigstore_and_creds(const char* fname, const char* server, amxc_string_t* usr, amxc_string_t* pwd, amxc_string_t* sigstore) {
    int ret = -1;
    amxc_var_t* sigstore_data = NULL;
    sigstore_data = read_config(fname);
    if(sigstore_data) {
        amxc_var_t* var_server = GET_ARG(sigstore_data, server);
        if(var_server) {
            amxc_var_t* var_sigstore = amxc_var_get_path(var_server, "sigstore", AMXC_VAR_FLAG_DEFAULT);
            amxc_string_set(sigstore, amxc_var_constcast(cstring_t, var_sigstore));
            amxc_var_t* var_usr = amxc_var_get_path(var_server, "username", AMXC_VAR_FLAG_DEFAULT);
            amxc_string_set(usr, amxc_var_constcast(cstring_t, var_usr));
            amxc_var_t* var_pwd = amxc_var_get_path(var_server, "password", AMXC_VAR_FLAG_DEFAULT);
            amxc_string_set(pwd, amxc_var_constcast(cstring_t, var_pwd));
            ret = 0;

            amxc_var_delete(&var_pwd);
            amxc_var_delete(&var_usr);
            amxc_var_delete(&var_sigstore);
        }
        amxc_var_delete(&var_server);
    }
    amxc_var_delete(&sigstore_data);
    return ret;
}

static int fetch_signature(const rlyeh_signature_data_t* sigdata,
                           const char* sigpath) {
    int ret = -1;
    char* prefix_url = strdup("https://");
    amxc_string_t url;
    amxc_string_t usr;
    amxc_string_t pwd;
    curl_off_t* size = NULL;
    const char* signature_url_overload = sigdata->signature_url_overload;
    rlyeh_curl_parameters_t curl_param = {
        .username = NULL,
        .password = NULL,
        .token = NULL,
        .type = FILE_TYPE_SIG,
        .auth_type = NO_AUTHENTICATION
    };

    amxc_string_init(&usr, 256);
    amxc_string_init(&pwd, 256);
    amxc_string_init(&url, 256);

    if(signature_url_overload) {
        size_t signature_url_overload_len = strlen(signature_url_overload);
        amxc_string_set_at(&url, 0, signature_url_overload, signature_url_overload_len, amxc_string_no_flags);
    } else {
        if(retrieve_sigstore_and_creds("/etc/amx/librlyeh/sigstore.json", sigdata->server, &usr, &pwd, &url)) {
            SAH_TRACEZ_ERROR(ME, "Cannot get sigstore credentials for server: '%s'", sigdata->server);
            goto exit;
        } else {
            curl_param.auth_type = BASIC_AUTHENTICATION;
            amxc_string_set_at(&url, 0, prefix_url, strlen(prefix_url), amxc_string_no_flags);
            amxc_string_appendf(&url, "%s-%s.signature", sigdata->image_name, sigdata->version);
        }
    }

    SAH_TRACEZ_INFO(ME, "Fetch signature %s", url.buffer);

    FILE* fd = fopen(sigpath, "w");
    ret = rlyeh_curl_download_content(&curl_param, &url, (void*) fd, size, NULL);
    fclose(fd);

exit:
    amxc_string_clean(&url);
    amxc_string_clean(&usr);
    amxc_string_clean(&pwd);
    free(prefix_url);
    return ret;
}

int extract_and_verify_signature(const char* sigpath, const char* jsonname) {
    gpgme_ctx_t ctx;
    gpgme_error_t err;
    gpgme_data_t signature = NULL;
    gpgme_data_t plain = NULL;
    gpgme_verify_result_t vresult;
    FILE* fd = NULL;
    int ret = -1;

    ret = init_gpgme(GPGME_PROTOCOL_OpenPGP);
    if(ret) {
        SAH_TRACEZ_ERROR(ME, "Cannot initialize GPGME");
        goto exit;
    }

    err = gpgme_new(&ctx);
    if(err != GPG_ERR_NO_ERROR) {
        print_err(err);
        ret = -2;
        goto exit;
    }

    /* Retrieve data from signature filename to operate on. */
    err = gpgme_data_new_from_file(&signature, sigpath, 1);
    if(err != GPG_ERR_NO_ERROR) {
        print_err(err);
        ret = -3;
        goto exitctx;
    }

    /* Verify signature. */
    err = gpgme_data_new(&plain);
    if(err != GPG_ERR_NO_ERROR) {
        print_err(err);
        ret = -4;
        goto exitctx;
    }

    err = gpgme_op_verify(ctx, signature, NULL, plain);
    if(err != GPG_ERR_NO_ERROR) {
        print_err(err);
        ret = -5;
        goto exitplain;
    }

    vresult = gpgme_op_verify_result(ctx);
    if(!(vresult->signatures->summary & GPGME_SIGSUM_VALID)) {
        err = GPG_ERR_BAD_SIGNATURE;
        SAH_TRACEZ_ERROR(ME, "Signature is not valid");
        ret = -255;
        goto exitsignature;
    }

    fd = fopen(jsonname, "wb");
    print_data(plain, fd);
    fclose(fd);

    ret = 0;
exitsignature:
    gpgme_data_release(signature);
exitplain:
    gpgme_data_release(plain);
exitctx:
    gpgme_release(ctx);
exit:
    return ret;
}

int check_docker_reference(amxc_var_t* obj, const rlyeh_signature_data_t* sigdata) {
    int ret = -3;
    amxc_var_t* var_docker_reference;
    const char* str_docker_reference;
    amxc_string_t expected_docker_reference;
    amxc_string_init(&expected_docker_reference, 256);
    amxc_string_setf(&expected_docker_reference, "%s/%s:%s", sigdata->server, sigdata->image_name, sigdata->version);

    amxc_var_t* identity = GET_ARG(obj, "identity");
    if(!identity) {
        SAH_TRACEZ_ERROR(ME, "Wrong key in json signature");
        goto exit;
    }
    var_docker_reference = amxc_var_get_path(identity, OCI_SIGNATURE_DOCKER_REFERENCE, AMXC_VAR_FLAG_DEFAULT);
    if(var_docker_reference == NULL) {
        SAH_TRACEZ_ERROR(ME, OCI_SIGNATURE_DOCKER_REFERENCE " missing");
        goto exit;
    }

    str_docker_reference = amxc_var_constcast(cstring_t, var_docker_reference);
    if((str_docker_reference == NULL) || (str_docker_reference[0] == '\0')) {
        SAH_TRACEZ_ERROR(ME, OCI_SIGNATURE_DOCKER_REFERENCE " is empty");
        goto exit;
    }

    SAH_TRACEZ_WARNING(ME, "Expecting '%s' for " OCI_SIGNATURE_DOCKER_REFERENCE, expected_docker_reference.buffer);
    if(strcmp(str_docker_reference, expected_docker_reference.buffer) != 0) {
        SAH_TRACEZ_ERROR(ME, "Invalid " OCI_SIGNATURE_DOCKER_REFERENCE " in signature: '%s'", str_docker_reference);
        // fprintf(stderr, "docker reference KO \n[%s]\n[%s]\n", str_docker_reference, expected_docker_reference.buffer);
        ret = 3;
        goto exit;
    }

    ret = 0;
exit:
    amxc_string_clean(&expected_docker_reference);
    return ret;
}

int check_manifest_hash(amxc_var_t* obj, const rlyeh_signature_data_t* sigdata) {
    int ret = -4;
    amxc_var_t* var_docker_manifest_digest;
    const char* str_docker_manifest_digest;
    amxc_string_t expected_manifest_hash;
    amxc_string_init(&expected_manifest_hash, 0);
    amxc_string_setf(&expected_manifest_hash, "sha256:%s", sigdata->manifest_hash);

    amxc_var_t* image = GET_ARG(obj, "image");
    if(!image) {
        SAH_TRACEZ_ERROR(ME, "Wrong key in json signature");
        goto exit;
    }
    var_docker_manifest_digest = amxc_var_get_path(image, OCI_SIGNATURE_DOCKER_MANIFEST_DIGEST, AMXC_VAR_FLAG_DEFAULT);
    if(var_docker_manifest_digest == NULL) {
        SAH_TRACEZ_ERROR(ME, OCI_SIGNATURE_DOCKER_MANIFEST_DIGEST " missing");
        goto exit;
    }

    str_docker_manifest_digest = amxc_var_constcast(cstring_t, var_docker_manifest_digest);
    if((str_docker_manifest_digest == NULL) || (str_docker_manifest_digest[0] == '\0')) {
        SAH_TRACEZ_ERROR(ME, OCI_SIGNATURE_DOCKER_MANIFEST_DIGEST " is empty");
        goto exit;
    }

    SAH_TRACEZ_WARNING(ME, "Expecting '%s' for " OCI_SIGNATURE_DOCKER_MANIFEST_DIGEST, expected_manifest_hash.buffer);
    if(strcmp(str_docker_manifest_digest, expected_manifest_hash.buffer) != 0) {
        SAH_TRACEZ_ERROR(ME, "Invalid " OCI_SIGNATURE_DOCKER_MANIFEST_DIGEST " in signature: '%s'", str_docker_manifest_digest);
        // fprintf(stderr, "manifest hash KO \n[%s]\n[%s]\n", str_docker_manifest_digest, expected_manifest_hash.buffer);
        ret = 4;
        goto exit;
    }

    ret = 0;
exit:
    amxc_string_clean(&expected_manifest_hash);
    return ret;
}

int check_type(amxc_var_t* obj) {
    int ret = -5;
    const char* expected_type = "atomic container signature";

    amxc_var_t* var_type = amxc_var_get_path(obj, OCI_SIGNATURE_TYPE, AMXC_VAR_FLAG_DEFAULT);
    if(var_type == NULL) {
        SAH_TRACEZ_ERROR(ME, OCI_SIGNATURE_TYPE " missing");
        goto exit;
    }

    const char* str_type = amxc_var_constcast(cstring_t, var_type);
    if((str_type == NULL) || (str_type[0] == '\0')) {
        SAH_TRACEZ_ERROR(ME, OCI_SIGNATURE_TYPE " is empty");
        goto exit;
    }

    SAH_TRACEZ_WARNING(ME, "Expecting '%s' for " OCI_SIGNATURE_TYPE, expected_type);
    if(strcmp(str_type, expected_type) != 0) {
        SAH_TRACEZ_ERROR(ME, "Invalid " OCI_SIGNATURE_TYPE " in signature: '%s'", str_type);
        // fprintf(stderr, "Types KO \n[%s]\n[%s]\n", str_type, expected_type);
        ret = 5;
        goto exit;
    }

    ret = 0;
exit:
    return ret;
}

int signature_content_verification(const char* jsonfile, const rlyeh_signature_data_t* sigdata) {
    int ret = 0;
    amxc_var_t* config_data = NULL;
    amxc_var_t* critical = NULL;

    config_data = read_config(jsonfile);
    if(!config_data) {
        SAH_TRACEZ_ERROR(ME, "Can't parse signature");
        ret = -1;
        goto exit;
    }
    // Parse data here and compare to docker reference and digest
    critical = GET_ARG(config_data, "critical");
    if(!critical) {
        SAH_TRACEZ_ERROR(ME, "Wrong key in json signature");
        ret = -2;
        goto exit;
    }

    ret = check_docker_reference(critical, sigdata);
    when_failed(ret, exit);

    ret = check_manifest_hash(critical, sigdata);
    when_failed(ret, exit);

    ret = check_type(critical);
    when_failed(ret, exit);

exit:
    amxc_var_delete(&config_data);

    return ret;
}


void rlyeh_signature_data_init(rlyeh_signature_data_t* data) {
    data->uri = NULL;
    data->transport = NULL;
    data->server = NULL;
    data->image_name = NULL;
    data->version = NULL;
    data->manifest_hash = NULL;
    data->signature_url_overload = NULL;
}

void rlyeh_signature_data_clean(rlyeh_signature_data_t* data) {
    if(!data) {
        return;
    }
    free(data->uri);
    free(data->transport);
    free(data->server);
    free(data->image_name);
    free(data->version);
    free(data->manifest_hash);
    free(data->signature_url_overload);
}

int rlyeh_verify_signature(const rlyeh_signature_data_t* sigdata) {
    int ret = 0;
    char* localsigpath = tempnam(RLYEH_TMP_DIR, "signa");
    char* localjsonname = tempnam(RLYEH_TMP_DIR, "sigda");

    SAH_TRACEZ_INFO(ME, "localsigpath: '%s' | localjsonname: '%s'", localsigpath, localjsonname);

    // Fetch signature https://<sigstore>/<image-name>:<version>.signature
    ret = fetch_signature(sigdata, localsigpath);
    if(ret) {
        SAH_TRACEZ_ERROR(ME, "Can't fetch the signature for image %s", sigdata->image_name);
        goto exit;
    }

    // Verify signature
    // public key previously (and manually) verified with good trust and validity
    ret = extract_and_verify_signature(localsigpath, localjsonname);
    if(ret) {
        SAH_TRACEZ_ERROR(ME, "Can't extract signature (%d)", ret);
        goto exit;
    }

    // Check manifest signature
    // docker reference
    // manifest digest
    ret = signature_content_verification(localjsonname, sigdata);
    if(ret) {
        SAH_TRACEZ_ERROR(ME, "Invalid signature (%d)", ret);
        goto exit;
    }

exit:
    // Delete signature
    if(file_exists(localsigpath)) {
        SAH_TRACEZ_INFO(ME, "Removing: '%s'", localsigpath);
        remove(localsigpath);
    }
    if(file_exists(localjsonname)) {
        SAH_TRACEZ_INFO(ME, "Removing: '%s'", localjsonname);
        remove(localjsonname);
    }
    free(localsigpath);
    free(localjsonname);

    return ret;
}